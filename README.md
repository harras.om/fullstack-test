#  Informations about the services and how to set up.

This project contains a set up of back end services and one front end to display the data.
I'm gonna list the purpose of each one and tell you how to start it.

# Installing / Getting Started
These instructions will get you a copy of the project up and running on your local machine.

clone this repo using : `git clone https://gitlab.com/harras.om/fullstack-test.git`

## Prerequisites

In order to start the back end services, you'll need a bunch of tools installed in your machine, which are : 

 - [Golang](https://golang.org/doc/install) 

> Make sure that the go binaries are in your `PATH` (see installation instructions)

 - In order to perform code generation in your machine, you will need to install `protoc` on Mac OS you can run the command : `brew install protobuf`
 - [NodeJS](https://nodejs.org/en/download/)

#### Running services :
To run each `task-services`  `service-registery` and `frontend` go inside the project directory and run : 

 - `npm i`
 - `npm start`

to run `freelancers-service` go inside the project directory and run :
- `go run freelancers-service/freelancer_server/server.go` for the server
- `go run freelancers-service/freelancer_server/client.go` for the client

## Assets


| Asset Name|Description  |
|--|-
|**service-registry** | The service registry in a microservices architecture is a database populated with information on how to dispatch requests to microservice instances. so in this project there is an example on how to register a service and listen to it. Each time wi start the `tasks-services` our service registry is ready to listen to it and give us information.
| **tasks-service** |this service is the backend responsible for manipulating data about tasks, like serving it to the frontend or posting any change to the db |
|--|--|
| **freelancers-service server** | this is another microservice written in golang and protobuf to generate data for use in communication protocol. with protocol Buffers we define data the messages(data request and response) and also we define Service (Service name and endpoint). In our case we're defining messages about freelancers in form of proto req/res |
| **frontend** | the frontend is a React web application that lists all the tasks available for freelancers, and give the ability to the Ops to notify freelancers if they are qualified for the task.|


## Ressources

Here is a list of some ressources that helped me :

 - https://auth0.com/blog/an-introduction-to-microservices-part-3-the-service-registry/
 - https://developers.google.com/protocol-buffers/docs/gotutorial
 - https://medium.com/pantomath/how-we-use-grpc-to-build-a-client-server-system-in-go-dd20045fa1c2

**Finally**
the order to start services is : 

 1. service-registry
 2. tasks-service
 3. freelancers-service
	 4. server.go
	 5. client.go
6. frontend 

PS : there are some features that are not finished yet.

