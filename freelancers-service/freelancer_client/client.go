package main

import (
	"context"
	"fmt"
	"log"

	freelancerspb "../freelancerpb"
	"google.golang.org/grpc"
)

func main() {
	fmt.Println("Freelancers service Client")

	opts := grpc.WithInsecure()

	cc, err := grpc.Dial("localhost:50051", opts)
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer cc.Close()

	c := freelancerspb.NewFreelancerServiceClient(cc)

	// Get Freelancer Info
	fmt.Println("Getting freelancers info...")

	//res, err := c.GetFreelancersInfo(context.Background(), &freelancerspb.GetFreelancersInfoRequest{Id: "test"})
	//if err != nil {
	//	fmt.Printf("Error happened: %v", err)
	//}

	getInfoReq := &freelancerspb.GetFreelancersInfoRequest{Id: "22Bw49hoRFhd8bc2r"}
	getInfoRes, getInfoErr := c.GetFreelancersInfo(context.Background(), getInfoReq)

	if getInfoErr != nil {
		fmt.Printf("Error happened while getting info: %v", getInfoErr)
	}

	fmt.Printf("Info about the freelancers: %v", getInfoRes)
}
