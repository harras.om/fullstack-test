// Code generated by protoc-gen-go. DO NOT EDIT.
// source: freelancerpb/freelancer.proto

package freelancerspb

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type Freelancer struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Tag                  []string `protobuf:"bytes,2,rep,name=tag,proto3" json:"tag,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Freelancer) Reset()         { *m = Freelancer{} }
func (m *Freelancer) String() string { return proto.CompactTextString(m) }
func (*Freelancer) ProtoMessage()    {}
func (*Freelancer) Descriptor() ([]byte, []int) {
	return fileDescriptor_2f064464e59f48c8, []int{0}
}

func (m *Freelancer) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Freelancer.Unmarshal(m, b)
}
func (m *Freelancer) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Freelancer.Marshal(b, m, deterministic)
}
func (m *Freelancer) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Freelancer.Merge(m, src)
}
func (m *Freelancer) XXX_Size() int {
	return xxx_messageInfo_Freelancer.Size(m)
}
func (m *Freelancer) XXX_DiscardUnknown() {
	xxx_messageInfo_Freelancer.DiscardUnknown(m)
}

var xxx_messageInfo_Freelancer proto.InternalMessageInfo

func (m *Freelancer) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *Freelancer) GetTag() []string {
	if m != nil {
		return m.Tag
	}
	return nil
}

type GetFreelancersInfoRequest struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetFreelancersInfoRequest) Reset()         { *m = GetFreelancersInfoRequest{} }
func (m *GetFreelancersInfoRequest) String() string { return proto.CompactTextString(m) }
func (*GetFreelancersInfoRequest) ProtoMessage()    {}
func (*GetFreelancersInfoRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_2f064464e59f48c8, []int{1}
}

func (m *GetFreelancersInfoRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetFreelancersInfoRequest.Unmarshal(m, b)
}
func (m *GetFreelancersInfoRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetFreelancersInfoRequest.Marshal(b, m, deterministic)
}
func (m *GetFreelancersInfoRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetFreelancersInfoRequest.Merge(m, src)
}
func (m *GetFreelancersInfoRequest) XXX_Size() int {
	return xxx_messageInfo_GetFreelancersInfoRequest.Size(m)
}
func (m *GetFreelancersInfoRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_GetFreelancersInfoRequest.DiscardUnknown(m)
}

var xxx_messageInfo_GetFreelancersInfoRequest proto.InternalMessageInfo

func (m *GetFreelancersInfoRequest) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

type GetFreelancersInfoResponse struct {
	Freelancer           *Freelancer `protobuf:"bytes,1,opt,name=freelancer,proto3" json:"freelancer,omitempty"`
	XXX_NoUnkeyedLiteral struct{}    `json:"-"`
	XXX_unrecognized     []byte      `json:"-"`
	XXX_sizecache        int32       `json:"-"`
}

func (m *GetFreelancersInfoResponse) Reset()         { *m = GetFreelancersInfoResponse{} }
func (m *GetFreelancersInfoResponse) String() string { return proto.CompactTextString(m) }
func (*GetFreelancersInfoResponse) ProtoMessage()    {}
func (*GetFreelancersInfoResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_2f064464e59f48c8, []int{2}
}

func (m *GetFreelancersInfoResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetFreelancersInfoResponse.Unmarshal(m, b)
}
func (m *GetFreelancersInfoResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetFreelancersInfoResponse.Marshal(b, m, deterministic)
}
func (m *GetFreelancersInfoResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetFreelancersInfoResponse.Merge(m, src)
}
func (m *GetFreelancersInfoResponse) XXX_Size() int {
	return xxx_messageInfo_GetFreelancersInfoResponse.Size(m)
}
func (m *GetFreelancersInfoResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_GetFreelancersInfoResponse.DiscardUnknown(m)
}

var xxx_messageInfo_GetFreelancersInfoResponse proto.InternalMessageInfo

func (m *GetFreelancersInfoResponse) GetFreelancer() *Freelancer {
	if m != nil {
		return m.Freelancer
	}
	return nil
}

func init() {
	proto.RegisterType((*Freelancer)(nil), "freelancers.Freelancer")
	proto.RegisterType((*GetFreelancersInfoRequest)(nil), "freelancers.GetFreelancersInfoRequest")
	proto.RegisterType((*GetFreelancersInfoResponse)(nil), "freelancers.GetFreelancersInfoResponse")
}

func init() { proto.RegisterFile("freelancerpb/freelancer.proto", fileDescriptor_2f064464e59f48c8) }

var fileDescriptor_2f064464e59f48c8 = []byte{
	// 192 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x92, 0x4d, 0x2b, 0x4a, 0x4d,
	0xcd, 0x49, 0xcc, 0x4b, 0x4e, 0x2d, 0x2a, 0x48, 0xd2, 0x47, 0x70, 0xf4, 0x0a, 0x8a, 0xf2, 0x4b,
	0xf2, 0x85, 0xb8, 0x11, 0x22, 0xc5, 0x4a, 0x7a, 0x5c, 0x5c, 0x6e, 0x70, 0xae, 0x10, 0x1f, 0x17,
	0x53, 0x66, 0x8a, 0x04, 0xa3, 0x02, 0xa3, 0x06, 0x67, 0x10, 0x53, 0x66, 0x8a, 0x90, 0x00, 0x17,
	0x73, 0x49, 0x62, 0xba, 0x04, 0x93, 0x02, 0xb3, 0x06, 0x67, 0x10, 0x88, 0xa9, 0xa4, 0xcd, 0x25,
	0xe9, 0x9e, 0x5a, 0x82, 0xd0, 0x52, 0xec, 0x99, 0x97, 0x96, 0x1f, 0x94, 0x5a, 0x58, 0x9a, 0x5a,
	0x5c, 0x82, 0xae, 0x5d, 0x29, 0x94, 0x4b, 0x0a, 0x9b, 0xe2, 0xe2, 0x82, 0xfc, 0xbc, 0xe2, 0x54,
	0x21, 0x73, 0x2e, 0x2e, 0x84, 0x4b, 0xc0, 0xba, 0xb8, 0x8d, 0xc4, 0xf5, 0x90, 0x1c, 0xa7, 0x87,
	0xd0, 0x19, 0x84, 0xa4, 0xd4, 0xa8, 0x8a, 0x4b, 0x10, 0x21, 0x13, 0x9c, 0x5a, 0x54, 0x96, 0x99,
	0x9c, 0x2a, 0x94, 0xca, 0x25, 0x84, 0x69, 0x97, 0x90, 0x1a, 0x8a, 0x79, 0x38, 0x5d, 0x2e, 0xa5,
	0x4e, 0x50, 0x1d, 0xc4, 0xd1, 0x4e, 0xfc, 0x51, 0xbc, 0x48, 0x2a, 0x0b, 0x92, 0x92, 0xd8, 0xc0,
	0x81, 0x6a, 0x0c, 0x08, 0x00, 0x00, 0xff, 0xff, 0x56, 0x2c, 0xea, 0x89, 0x75, 0x01, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// FreelancerServiceClient is the client API for FreelancerService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type FreelancerServiceClient interface {
	GetFreelancersInfo(ctx context.Context, in *GetFreelancersInfoRequest, opts ...grpc.CallOption) (*GetFreelancersInfoResponse, error)
}

type freelancerServiceClient struct {
	cc *grpc.ClientConn
}

func NewFreelancerServiceClient(cc *grpc.ClientConn) FreelancerServiceClient {
	return &freelancerServiceClient{cc}
}

func (c *freelancerServiceClient) GetFreelancersInfo(ctx context.Context, in *GetFreelancersInfoRequest, opts ...grpc.CallOption) (*GetFreelancersInfoResponse, error) {
	out := new(GetFreelancersInfoResponse)
	err := c.cc.Invoke(ctx, "/freelancers.FreelancerService/GetFreelancersInfo", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// FreelancerServiceServer is the server API for FreelancerService service.
type FreelancerServiceServer interface {
	GetFreelancersInfo(context.Context, *GetFreelancersInfoRequest) (*GetFreelancersInfoResponse, error)
}

// UnimplementedFreelancerServiceServer can be embedded to have forward compatible implementations.
type UnimplementedFreelancerServiceServer struct {
}

func (*UnimplementedFreelancerServiceServer) GetFreelancersInfo(ctx context.Context, req *GetFreelancersInfoRequest) (*GetFreelancersInfoResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetFreelancersInfo not implemented")
}

func RegisterFreelancerServiceServer(s *grpc.Server, srv FreelancerServiceServer) {
	s.RegisterService(&_FreelancerService_serviceDesc, srv)
}

func _FreelancerService_GetFreelancersInfo_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetFreelancersInfoRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FreelancerServiceServer).GetFreelancersInfo(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/freelancers.FreelancerService/GetFreelancersInfo",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FreelancerServiceServer).GetFreelancersInfo(ctx, req.(*GetFreelancersInfoRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _FreelancerService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "freelancers.FreelancerService",
	HandlerType: (*FreelancerServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetFreelancersInfo",
			Handler:    _FreelancerService_GetFreelancersInfo_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "freelancerpb/freelancer.proto",
}
