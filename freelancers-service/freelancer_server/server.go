package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"

	freelancerspb "../freelancerpb"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var collection *mongo.Collection

type server struct {
}

type freelancerItem struct {
	ID   primitive.ObjectID `bson:"_id,omitempty"`
	tags []string           `bson:"tags"`
}

func (*server) GetFreelancersInfo(ctx context.Context, req *freelancerspb.GetFreelancersInfoRequest) (*freelancerspb.GetFreelancersInfoResponse, error) {

	fmt.Println("Get freelancer info request")

	freelancerId := req.GetId()
	oid, err := primitive.ObjectIDFromHex(freelancerId)
	if err != nil {
		return nil, status.Errorf(
			codes.InvalidArgument,
			fmt.Sprintf("Cannot parse ID"),
		)
	}

	// Create an empty struct
	data := &freelancerItem{}
	// We want the object id to be equal to oid
	filter := bson.M{"_id": oid}

	res := collection.FindOne(context.Background(), filter)
	if err := res.Decode(data); err != nil {
		return nil, status.Errorf(
			codes.NotFound,
			fmt.Sprintf("ID not found : $v", err),
		)
	}

	return &freelancerspb.GetFreelancersInfoResponse{
		Freelancer: &freelancerspb.Freelancer{
			Id:  data.ID.Hex(),
			Tag: data.tags,
		},
	}, nil
}

func main() {
	// Log the file name and line number
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	fmt.Println("Connected to Mongodb")

	// Connect to mongo db
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		log.Fatal(err)
	}
	err = client.Connect(context.TODO())
	if err != nil {
		log.Fatal(err)
	}

	collection = client.Database("tags").Collection("freelancers")

	fmt.Println("Freelancers service started")

	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	opts := []grpc.ServerOption{}
	s := grpc.NewServer(opts...)
	freelancerspb.RegisterFreelancerServiceServer(s, &server{})

	go func() {
		fmt.Println("Starting server...")
		if err := s.Serve(lis); err != nil {
			log.Fatalf("Failed to start server: %v", err)
		}
	}()

	// Wait for Ctrl + c to exit
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)

	// Block until a signal is received
	<-ch
	fmt.Println("Stopping the server...")
	s.Stop()
	fmt.Println("Closing the listener")
	lis.Close()
	fmt.Println("Closing Mongodb connection...")
	client.Disconnect(context.TODO())
}
