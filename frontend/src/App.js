import React, { useState, useEffect } from "react";

import TasksList from "./components/Tasks/TasksList";
import axios from "axios";

function App() {
  const [tasks, setTasks] = useState([]);
  const [isVisible, setVisivle] = useState(false);

  // Fetch data once the component is rendered
  useEffect(() => {
    axios
      .get("http://localhost:49892/api/tasks/")
      .then(response => {
        const loadedTasks = [];
        for (let key in response.data) {
          loadedTasks.push({
            id: key,
            name: response.data[key].name
          });
        }
        // Set fetched data to the comp state
        setTasks(loadedTasks);
      })
      .catch(err => {
        console.log("Error occured : ", err);
      });
  }, []);

  // We should POST ID of the task and the value of the visible state
  const checkTaskHandler = (taskId, visible) => {
    setVisivle(!isVisible);
    const obj = {
      taskId: taskId,
      visible: visible
    };
    axios
      .post("http://localhost:49892/api/tasks/", obj)
      .then(response => {
        console.log("response : ", response);
      })
      .catch(err => {
        console.log(err);
      });
  };

  // Pass the id of and value to onClickHandler
  return (
    <div className="App">
      <TasksList tasks={tasks} onClick={event => checkTaskHandler(event)} />
    </div>
  );
}

export default App;
