const express = require("express");

const { Tasks } = require("./lib/tasks");

const service = express();

module.exports = config => {
  const log = config.log();
  // Add a request logging middleware in development mode
  if (service.get("env") === "development") {
    service.use((req, res, next) => {
      log.debug(`${req.method}: ${req.url}`);
      return next();
    });
  }

  service.get("/api/tasks", async (req, res, next) => {
    const tasks = await Tasks.find({}).select("name");
    res.send(tasks);
  });

  service.put("/api/tasks/:id", async (req, res) => {
    try {
      const task = await Tasks.findByIdAndUpdate(
        req.params.id,
        { visible: req.body.visible },
        { new: true }
      );
      if (!task) res.status(404).send("Not found");
      task.visible = req.body.visible;
      res.send(task);
    } catch (error) {
      throw new Error(error);
    }
  });

  service.use((error, req, res, next) => {
    res.status(error.status || 500);
    // Log out the error to the console
    log.error(error);
    return res.json({
      error: {
        message: error.message
      }
    });
  });
  return service;
};
