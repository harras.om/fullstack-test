const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const Tasks = mongoose.model(
  "Tasks",
  new mongoose.Schema({
    _id: Schema.Types.ObjectId,
    name: String,
    type: {
      category: String,
      key: String,
      fr: {
        translatedName: String,
        description: String,
        TranslationCategory: String
      },
      en: {
        translatedName: String,
        description: String,
        TranslationCategory: String
      }
    },
    shifts: [
      {
        _id: Schema.Types.ObjectId,
        break: Number,
        seats: Number,
        time: {
          startDate: Date,
          endDate: Date
        }
      }
    ],
    visible: Boolean,
    country: String,
    pricing: {
      side: Number,
      sider: Number,
      currency: String
    },
    createdAt: Date,
    updatedAt: Date
  })
);

module.exports.Tasks = Tasks;
